#!/bin/sh
# A custom install prefix can be given as argument, eg.
# './install.sh /usr' default prefix is set below.
PREFIX=${1:-$HOME/.local}

umask 0022

python src/setup.py install --prefix ${PREFIX}
install -m 755 -d ${PREFIX}/bin ${PREFIX}/share/doc/metaimage
install -m 644 README ${PREFIX}/share/doc/metaimage/

cd ${PREFIX}/bin
find ../ -type f -path '*/metaimage/__main__.py' \
  -exec chmod 755 {} \; -exec ln -sf {} ./metaimage \;
