#!/bin/sh
# setup enviroment for make

VERSION_FILE=../$(grep "verfile = " ../src/setup.py | cut -d\' -f2)
GIT_REVISION=$(git rev-parse HEAD)
GIT_REVSHORT=$(echo ${GIT_REVISION} | cut -b 1-12)
PKG_VERSION=$(grep "__version__ = " "${VERSION_FILE}" | cut -d\' -f2)
PKG_RELEASE=$(grep 'pkgrel=' pkgbuild.sh | cut -d\' -f2)

[ -n "$1" ] || exit
mkdir -p $(dirname $1)

cat > $1 << EOF
export PKG_VERSION:=${PKG_VERSION}
export PKG_RELEASE:=${PKG_RELEASE}
export GIT_REVISION:=${GIT_REVISION}
export GIT_REVSHORT:=${GIT_REVSHORT}
export VERSION_FILE:=${VERSION_FILE}
EOF
