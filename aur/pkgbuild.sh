#!/bin/sh
# generating PKGBUILD file for Archlinux

[ -n "${GIT_REVISION}" ] && [ -n "${GIT_REVSHORT}" ] &&
[ -n "${PKG_VERSION}" ] && [ -n "$1" ] || exit 1

build="\${startdir}/build"

cat > $1 << EOF
# Maintainer: Rasmus Thystrup Karstensen <rathka at gmail dot com>
# git-revision: ${GIT_REVISION}
pkgname='metaimage'
pkgver='${PKG_VERSION}'
pkgrel='1'
pkgdesc='Tool for batch-proccessing metadata in image files'
arch=(any)
url='https://gitlab.com/rathka/metaimage'
license=(GPL3)
source=(https://gitlab.com/rathka/metaimage/repository/archive.tar.bz2?ref=${GIT_REVISION})
depends=(python python-gobject python-pytz python-setuptools libgexiv2)
md5sums=(SKIP)

build() {
    mkdir -p ${build}
    cd \${srcdir}/metaimage-${GIT_REVISION}-${GIT_REVISION}
    export PYTHONPATH=${build}/usr
    python src/setup.py install --single-version-externally-managed --root ${build}
}

package() {
    mv ${build}/* \${pkgdir}/
}
EOF
