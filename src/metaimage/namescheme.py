#!/usr/bin/env python3

from re import finditer
from math import floor, log2
from datetime import datetime, timedelta

from metaimage.multiencode import Multiencoding, CustomBase

def _binlen(value):
    return floor(log2(value)) +1


class DateTimeEncoding(str):
    def __new__(cls, timestamp, fmt, encoder, prefix='', suffix=''):
        name = timestamp.strftime(fmt)
        seconds = round((timestamp - datetime.strptime(name, fmt)).total_seconds())
        time_bits = _binlen(fmt.precision)

        def magic(base, length):
            bits = _binlen(base**length -1) - time_bits
            return (length, bits) if bits > 0 else magic(base, length +1)

        obj = str.__new__(cls, prefix + name + suffix)
        obj.length, count_bits = magic(len(encoder), fmt.length)
        obj.timecount = Multiencoding([(seconds, time_bits), (0, count_bits)])
        obj.encoder = encoder
        return obj


    @property
    def _bits(self): # bits available with current digits
        return _binlen(len(self.encoder)**self.length -1)


    def __str__(self):
        timecount = self.encoder.encode(int(self.timecount))
        diff = self.length - len(timecount)
        if diff > 0:
            timecount = (self.encoder[0] * diff) + timecount
        return self.format(timecount)


    def __repr__(self):
        return self.format('{'+ str(self.length) +'}')


    def renew(self):
        time, tbits = self.timecount[0]
        count, cbits = self.timecount[1]
        count += 1
        if cbits < _binlen(count):
            self.length += 1
            cbits = self._bits - tbits
        self.timecount[1] = (count, cbits)



class Format(str):
    def __new__(cls, fmt, placeholder='{}'):
        obj = None
        for match in finditer('(?<!%)(%(?:%%)*){(\d+)}', fmt):
            if obj is None:
                obj = str.__new__(cls, fmt.replace(match[0], match[1][1:] + placeholder))
                obj._length = int(match[2])
            else:
                raise ValueError("format '{}' may not contain more than one '%{{int}}'".format(fmt))

        if obj is None:
            obj = str.__new__(cls, fmt)
            obj._length = None

        precision = None
        year   = 31536000
        month  = 2678400
        week   = 604800
        day    = 86400
        hour   = 3600
        minute = 60
        second = 1

        directives = list()
        for s in fmt.replace('%-', '%').split('%')[1:]:
            if s: directives.append(s[0])
        directives = set(directives)

        def contains(s):
            for c in s:
                if c in directives:
                    return True
            return False

        if contains('c'): # has full date and time
            precision = second
        elif contains('x'): # has full date
            precision = day
        elif contains('yY'): # has year
            precision = year
            if contains('j'): # has day of year
                precision = day
            if precision > month and contains('bBm'): # has month
                precision = month
                if contains('d'): # has day of month
                    precision = day
            if precision > week and contains('UW'): # has week
                precision = week
                if contains('aAw'): # has day of week
                    precision = day
        if precision == day:
            if contains('X'): # has full time
                precision = second
            elif contains('H') or (contains('I') and contains('p')): # has hour
                precision = hour
                if contains('M'): # has minute
                    precision = minute
                    if contains('S'): # has second
                        precision = second
        obj._precision = precision
        return obj


    @property
    def precision(self):
        return self._precision


    @property
    def length(self):
        return self._length

