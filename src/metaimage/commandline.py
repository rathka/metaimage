#!/usr/bin/env python3

import sys
from argparse import ArgumentParser
from metaimage import __version__
from metaimage.namescheme import Format
from metaimage.multiencode import CustomBase
from metaimage.gmapi import single_choice


parser = ArgumentParser(description='Process image metadata')
subparsers = parser.add_subparsers(title='actions', dest='action')

adjust_time = subparsers.add_parser('adjust',
    help='adjust metadata timestamps and rotation tag')
apply_preset = subparsers.add_parser('apply',
    help='apply preset to image files')
create_preset = subparsers.add_parser('create',
    help='create a skeleton preset')
geotag = subparsers.add_parser('geotag',
    help='add gpsinfo to metadata')
list_metadata = subparsers.add_parser('list',
    help='list image metadata')
rename = subparsers.add_parser('rename',
    help='rename files according to image timestamp')
utc_util = subparsers.add_parser('utc',
    help='calculate timestamp offset')

adjust_time.add_argument('-r', '--rotate',
    help='adjust rotation',
    type=int,
    default=0,
    choices=[0, 90, 180, 270],
    metavar='degree')
adjust_time.add_argument('--flip-horizontal', 
    help='flip horizontal',
    action='store_true',
    dest='flip_horizontal')
adjust_time.add_argument('--flip-vertical',
    help='flip vertical',
    action='store_true',
    dest='flip_vertical')
adjust_time.add_argument('-S', '--seconds',
    help='adjust seconds',
    type=int,
    default=0,
    metavar='seconds')
adjust_time.add_argument('-M', '--minutes',
    help='adjust minutes',
    type=float,
    default=0,
    metavar='minutes')
adjust_time.add_argument('-H', '--hours',
    help='adjust hours',
    type=float,
    default=0,
    metavar='hours')
adjust_time.add_argument('-D', '--days',
    help='adjust days',
    type=float,
    default=0,
    metavar='days')

apply_preset.add_argument('preset',
    help='read preset from file',
    type=str,
    metavar='preset')

create_preset.add_argument('image',
    help='read image file',
    type=str,
    metavar='image')

geotag.add_argument('query',
    help='lookup gps coordinates for address using google maps api',
    type=str,
    metavar='query')
geotag.add_argument('-c', '--coordinates',
    help='treat query as a set of coordinates: latitude,longitude',
    action='store_true')
geotag.add_argument('-f', '--force',
    help='overwrite existing geotag information',
    action='store_true')

list_metadata.add_argument('-k', '--keys',
    help='a comma delimitet list of keys to match',
    type=str,
    metavar='list')
list_metadata.add_argument('-v', '--values',
    help='a comma delimitet list of values to match',
    type=str,
    metavar='list')
list_metadata.add_argument('-i', '--imagename',
    help='prefix image names when listing',
    action='store_true')
list_metadata.add_argument('-s', '--suppress',
    help='suppress values longer than "length", default is 30',
    type=int,
    default=30,
    metavar='length')

utc_util.add_argument('-o', '--offset',
    help='print offset from current utc time',
    type=str,
    dest='timestamp',
    metavar='timestamp')

for p in [apply_preset, rename]:
    p.add_argument('-d', '--destination',
        help='output directory',
        type=str,
        metavar='folder')

for p in [adjust_time, apply_preset, geotag, rename]:
    p.add_argument('-l', '--log-file',
        help='name of log-file',
        type=str,
        dest='log',
        default='metaimage.log',
        metavar='file')

for p in [create_preset, rename]:
    p.add_argument('-A', '--alphabet',
        help='use alphabet when renaming image files',
        type=str,
        default='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        metavar='alphabet')
    p.add_argument('-F', '--format',
        help='use format when renaming image files',
        type=str,
        default='%Y%m%d-%{5}',
        metavar='format')
    p.add_argument('-p', '--prefix',
        help='set prefix for new filename',
        type=str,
        default='',
        metavar='string')
    p.add_argument('-s', '--suffix',
        help='set suffix for new filename',
        type=str,
        default='',
        metavar='string')

for p in [create_preset, geotag, rename]:
    p.add_argument('-u', '--utc-offset',
        help='use offset for metadata timestamp',
        type=float,
        dest='offset',
        default=0,
        metavar='minutes')

for p in [adjust_time, geotag, list_metadata, rename]:
    p.add_argument('-m', '--match',
        help='match camera make and model',
        type=str,
        metavar=('make', 'model'),
        nargs=2)

for p in [adjust_time, apply_preset, geotag, list_metadata, rename]:
    p.add_argument('image_list',
        help='one or more files to process',
        type=str,
        metavar='image',
        nargs='+')


## switch functions ##
def _default(args):
    args.stdout = sys.stdout
    args.stderr = sys.stderr
    return args

def _rename(args):
    args.alphabet = CustomBase(args.alphabet)

    if args.destination and args.destination[-1] != '/':
        args.destination += '/'

    args.format = Format(args.format)
    if not args.format.precision:
        raise ValueError("format must declare a year")
    if not args.format.length:
        raise ValueError("format must declare a counter: '%{n}', where n > 0")

    return _default(args)

def _geotag(args):
    args.interact = single_choice
    return _default(args)


## main parse function ##

def parse_args():
    if len(sys.argv) < 2:
        print('metaimage version', __version__)
        parser.parse_args(['-h'])

    args = parser.parse_args()
    return {
        'adjust' : _default,
        'apply'  : _rename,
        'create' : _rename,
        'geotag' : _geotag,
        'list'   : _default,
        'rename' : _rename,
        'utc'    : _default
    }[args.action](args)


if __name__ == '__main__':
    print(parse_args())

