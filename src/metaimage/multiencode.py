#!/usr/bin/env python3

def _sanitize(value, bit):
    if value < 0:
        raise ValueError('signed values not supported')
    if value > 2**bit:
        raise ValueError('insufficient bits [{}] for representing value [{}]'.format(bit, value))
    return value, bit


class Multiencoding(list):

    def __init__(self, *data):
        if len(data) > 1:
            raise TypeError('Multiencoding() takes at most 1 argument ({} given)'.format(len(data)))
        if len(data) > 0:
            super().__init__([_sanitize(v, b) for v, b in data[0]])

    @classmethod
    def decode(cls, value, bits):
        data = list()
        for bit in reversed(bits):
            remainder = value>>bit
            data.append((value - (remainder<<bit), bit))
            value = remainder
        return cls(reversed(data))

    def __int__(self):
        value, bit = self[0]
        for val, bit in self[1:]:
            value = (value<<bit) + val
        return value

    def __str__(self):
        return '{!r} => {}'.format(self, int(self))

    def __setitem__(self, index, tbl):
        super().__setitem__(index, _sanitize(*tbl))

    def append(self, value, bit):
        super().append(_sanitize(value, bit))

    def extend(self, data):
        super().extend([_sanitize(v, b) for v, b in data])

    @property
    def values(self):
        return [val for val, bit in self]

    @property
    def bits(self):
        return [bit for val, bit in self]


class CustomBase(str):

    def __new__(cls, alphabet):
        if len(alphabet) < 2:
            raise ValueError('alphabet requires at least two characters')
        obj = str.__new__(cls, alphabet)
        obj._index = dict([(c, i) for i, c in enumerate(alphabet)])
        return obj

    def encode(self, value):
        if value < 0:
            raise ValueError('negative values not supported')
        enc = lambda n, b: self[n] if n < b else enc(n//b, b) + self[n%b]
        return enc(value, len(self))

    def decode(self, string):
        value = 0
        base = len(self)
        for index, char in enumerate(reversed(string)):
            value += self._index[char] * base**index
        return value
