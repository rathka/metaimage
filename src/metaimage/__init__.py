#!/usr/bin/env python3

__version__ = '1.6.0'

import os, gi, logging
from datetime import datetime, timedelta
from fractions import Fraction
from pytz import utc

gi.require_version('GExiv2', '0.10')
from gi.repository import GExiv2, GLib

class Timestamp(datetime):

    @classmethod
    def parse_gpsinfo(cls, date, time):
        h, m, s  = [Fraction(s) for s in time.split()]
        delta = timedelta(microseconds=round(((h * 60 + m) * 60 + s) * 10**6))
        return (cls.strptime(date, '%Y:%m:%d') + delta).replace(tzinfo=utc)


    @classmethod
    def parse_datetime(cls, timestamp):
        return cls.strptime(timestamp, '%Y:%m:%d %H:%M:%S')


    @classmethod
    def from_datetime(cls, datetime):
        return cls.combine(datetime.date(), datetime.time())


    @property
    def gpsdate(self):
        return self.strftime('%Y:%m:%d')


    @property
    def gpstime(self):
        s = Fraction(self.second * 10**6 + self.microsecond, 10**6)
        return '{}/1 {}/1 {}/{}'.format(self.hour, self.minute, s.numerator, s.denominator)


    @property
    def datetime(self):
        return str(self)


    def __str__(self):
        return self.strftime('%Y:%m:%d %H:%M:%S')


    def __add__(self, delta):
        return self.from_datetime(super().__add__(delta))


    def __sub__(self, other):
        r = super().__sub__(other)
        return self.from_datetime(r) if isinstance(r, datetime) else r



class Image(GExiv2.Metadata):
    path = None
    def __init__(self, path):
        try:
            super().__init__(path)
            self.path = path
        except (OSError, GLib.GError) as err:
            raise OSError(path+': loading metadata failed\ '+ str(err))

        self.timestamp = self.get_gps_timestamp()
        if self.timestamp is None:
            for key in ['Exif.Image.DateTimeOriginal', 'Exif.Photo.DateTimeOriginal']:
                try:
                    self.timestamp = Timestamp.parse_datetime(self[key])
                    self.utc_offset(self['Exif.Image.TimeZoneOffset'])
                    break
                except KeyError:
                    continue


    def save(self):
        self.save_file(self.path)


    def get_gps_timestamp(self):
        try:
            date = self['Exif.GPSInfo.GPSDateStamp']
            time = self['Exif.GPSInfo.GPSTimeStamp']
            return Timestamp.parse_gpsinfo(date, time)
        except KeyError:
            return None


    def get_gps_location(self):
        lng, lat, alt = self.get_gps_info()
        if not 'Exif.GPSInfo.GPSLatitude' in self:
            lat = None
        if not 'Exif.GPSInfo.GPSLongitude' in self:
            lng = None
        if not 'Exif.GPSInfo.GPSAltitude' in self:
            alt = None
        if lat is None and lng is None and alt is None:
            return None
        return (lat, lng, alt)


    def set_gps_timestamp(self, timestamp):
        timestamp = timestamp.astimezone(utc)
        self['Exif.GPSInfo.GPSDateStamp'] = timestamp.gpsdate
        self['Exif.GPSInfo.GPSTimeStamp'] = timestamp.gpstime


    def set_gps_location(self, latitude, longitude, altitude):
        self.set_gps_info(longitude, latitude, altitude)


    def rename(self, newname):
        if newname == self.path:
            return True
        elif os.path.exists(newname):
            return False
        else:
            os.rename(self.path, newname)
            self.path = newname
            return True


    def utc_offset(self, minutes):
        delta = timedelta(minutes=minutes)
        self.timestamp = (self.timestamp - delta).replace(tzinfo=utc)


    def orientation(self, rotate=0, flip_vertical=False, flip_horizontal=False):
        rotate90 = { 1:8, 8:3, 3:6, 6:1, 2:7, 7:4, 4:5, 5:2 }
        flipV = { 1:2, 2:1, 3:4, 4:3, 5:8, 8:5, 6:7, 7:6 }
        flipH = { 1:4, 4:1, 2:3, 3:2, 5:6, 6:5, 7:8, 8:7 }
        if not rotate in (0, 90, 180, 270):
            raise ValueError('invalid rotation argument: {}, expected: [0, 90, 180, 270]'.format(rotate))
        try:
            orientation = self['Exif.Image.Orientation']
        except KeyError:
            raise RuntimeError('orientation tag missing')
        for i in range(0, rotate/90):
            orientation = rotate90[orientation]
        if flip_vertical:
            orientation = flipV[orientation]
        if flip_horizontal:
            orientation = flipH[orientation]
        self['Exif.Image.Orientation'] = orientation


    def __str__(self):
        return self.path


    def __repr__(self):
        return self.path
