#!/usr/bin/python

# this code is currently unused

from pytz import timezone
from gmapi import LocationInfo

class Google:
    def __init__(self, query, interact=None): 
        li = LocationInfo.fromaddress(query, timezone=True,
                                      elevation=True, interact)
        
        self.LOCATION = li.coordinates()
        self.TIMEZONE = timezone(li.timezone())
        self.ADDRESS  = li.find('GeocodeResponse/result/formatted_address').text

    def coordinates(self, timestamp):
        return self.LOCATION

    def satellites(self, timestamp):
        return 'maps.googleapis.com'

    def timezone(self, latitude, longitude, elevation):
        return self.TIMEZONE
