#!/usr/bin/env python3

import unittest

from metaimage import Timestamp

from pytz import utc
from datetime import datetime, timedelta
from fractions import Fraction


class ReferenceDatetime(datetime):

    @property
    def gpsdate(self):
        return self.strftime('%Y:%m:%d')

    @property
    def gpstime(self):
        h = self.hour
        m = self.minute
        s = Fraction(self.second * 10**6 + self.microsecond, 10**6)
        return '%i/1 %i/1 %i/%i' %(h, m, s.numerator, s.denominator)

    def __str__(self):
        return self.strftime('%Y:%m:%d %H:%M:%S')



class TestTimestampClassmethods(unittest.TestCase):

    def setUp(self):
        self.ref1 = ReferenceDatetime.now()
        self.ref2 = ReferenceDatetime.now().replace(microsecond=0)

    def test_parse_gpsinfo(self):
        self.assertEqual(self.ref1.replace(tzinfo=utc),
            Timestamp.parse_gpsinfo(self.ref1.gpsdate, self.ref1.gpstime))
        self.assertEqual(self.ref2.replace(tzinfo=utc),
            Timestamp.parse_gpsinfo(self.ref2.gpsdate, self.ref2.gpstime))

        # test overflow
        self.assertEqual(datetime(1970, 1, 1, 1, 2, 30, 500000, tzinfo=utc),
            Timestamp.parse_gpsinfo('1970:1:1' , '1/1 1/1 905/10'))
        self.assertEqual(datetime(1970, 1, 1, 2, 2, 30, 500000, tzinfo=utc),
            Timestamp.parse_gpsinfo('1970:1:1' , '1/1 61/1 905/10'))
        self.assertEqual(datetime(1970, 1, 2, 2, 2, 30, 500000, tzinfo=utc),
            Timestamp.parse_gpsinfo('1970:1:1' , '25/1 61/1 905/10'))

    def test_parse_datetime(self):
        self.assertEqual(self.ref2,
            Timestamp.parse_datetime(str(self.ref2)))

    def test_from_datetime(self):
        self.assertTrue(self.ref1, Timestamp.from_datetime(self.ref1))
        self.assertTrue(self.ref2, Timestamp.from_datetime(self.ref2))



class TestTimestampStringMethods(unittest.TestCase):

    def setUp(self):
        self.ref1 = ReferenceDatetime.now()
        self.ref2 = ReferenceDatetime.now().replace(microsecond=0)
        self.ts1 = Timestamp.from_datetime(self.ref1)
        self.ts2 = Timestamp.from_datetime(self.ref2)

    def test_gpsdate(self):
        self.assertEqual(self.ref1.gpsdate, self.ts1.gpsdate)
        self.assertEqual(self.ref2.gpsdate, self.ts2.gpsdate)

    def test_gpstime(self):
        self.assertEqual(self.ref1.gpstime, self.ts1.gpstime)
        self.assertEqual(self.ref2.gpstime, self.ts2.gpstime)

    def test_str(self):
        self.assertEqual(str(self.ref1), str(self.ts1))
        self.assertEqual(str(self.ref2), str(self.ts2))



class TestTimestampAlgebraMethod(unittest.TestCase):

    def setUp(self):
        self.delta = 15
        ref = datetime.now()
        self.ref1 = ref.replace(second=self.delta)
        self.ref2 = ref.replace(second=self.delta*3)
        self.ts = Timestamp.from_datetime(ref.replace(second=self.delta*2))

    def test_timedeltaSubstraction(self):
        ts = self.ts - timedelta(seconds=self.delta)
        self.assertIsInstance(ts, Timestamp)
        self.assertEqual(ts, self.ref1)

    def test_timedeltaAddition(self):
        ts = self.ts + timedelta(seconds=self.delta)
        self.assertIsInstance(ts, Timestamp)
        self.assertEqual(ts, self.ref2)

    def test_datetimeSubstraction(self):
        delta = self.ts - self.ref1
        self.assertIsInstance(delta, timedelta)
        self.assertEqual(delta.total_seconds(), self.delta)
