#!/usr/bin/env python3

import unittest

from math import floor, log2

from metaimage.multiencode import Multiencoding, CustomBase


class TestMultiencoding(unittest.TestCase):

    def test_encode_decode(self):
        b1 = 8 # bit
        encoding = Multiencoding([(86000, 17), (0, b1), (255, 8), (0, 0)])
        for v2 in range(2**b1):
            b2 = floor(log2(v2)) +1 if v2 > 0 else 0
            for v1 in range(2**b1):
                encoding[1] = v1, b1
                encoding[3] = v2, b2
                decoding = Multiencoding.decode(int(encoding), encoding.bits)
                self.assertEqual(encoding, decoding)

    def test_init_error(self):
        with self.assertRaises(ValueError):
            Multiencoding([(86000, 17, 1)])
        with self.assertRaises(ValueError):
            Multiencoding([(8, 2)])

    def test_modification_error(self):
        encoding = Multiencoding([(86000, 17), (86370, 17), (511, 10)])
        with self.assertRaises(ValueError):
            encoding.append(512, 8)
        with self.assertRaises(ValueError):
            encoding[0] = 512, 8



class TestAnyBaseEncoding(unittest.TestCase):

    def test_init_error(self):
        for s in ('', '0'):
            with self.assertRaises(ValueError):
                CustomBase(s)

    def test_encode_decode(self):
        custom_base = CustomBase('0123456789abcdefghijklmnopqrstuvwxyz')
        value1 = 98765432109875654321
        value2 = custom_base.decode(custom_base.encode(value1))
        self.assertEqual(value1, value2)

