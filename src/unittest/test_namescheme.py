#!/usr/bin/env python3

import unittest

from math import floor, log2
from datetime import datetime, timedelta

from metaimage.multiencode import CustomBase
from metaimage.namescheme import Format, DateTimeEncoding


class TestFormat(unittest.TestCase):

    def setUp(self):
        self.second = 1
        self.minute = 60 * self.second
        self.hour = 60 * self.minute
        self.day = 24 * self.hour
        self.week = 7 * self.day
        self.month = 31 * self.day
        self.year = 365 * self.day


    def test_abuse(self):
        fmt = Format('')
        assert fmt.length is None
        assert fmt.precision is None

        fmt='%{1}%{2}'
        with self.assertRaises(ValueError):
            Format(fmt)


    def test_length(self):
        for i in range(0,100):
            fmt = Format('%{{{}}}'.format(i))
            self.assertEqual(fmt.length, i)

        fmt = Format('%%{0}')
        assert fmt.length is None

        fmt = Format('%%%{0}')
        self.assertEqual(fmt.length, 0)


    def test_precision(self):

        for fmt in ('%Y', '%y'):
            self.assertEqual(Format(fmt).precision, self.year)

        for fmt in ('%Y%m%d', '%Y%w%W', '%y%j', '%x'):
            self.assertEqual(Format(fmt).precision, self.day)

        for fmt in ('%Y%m%d%H%M%S', '%x%X', '%c'):
            self.assertEqual(Format(fmt).precision, self.second)

        assert Format('%j').precision is None
        self.assertEqual(Format('%Y%d').precision, self.year)
        self.assertEqual(Format('%Y%-m%-d').precision, self.day)


    def test_intended_use(self):
        fmt = Format('%Y%m%d-%H%M_%{5}')
        self.assertEqual(fmt.precision, self.minute)
        self.assertEqual(fmt.length, 5)

        fmt = Format('prefix_%Y%m%d-%{5}_suffix')
        self.assertEqual(fmt.precision, self.day)
        self.assertEqual(fmt.length, 5)



class TestDateTimeEncoding(unittest.TestCase):

    def setUp(self):
        self.timestamp = datetime.now()
        self.encoder = CustomBase('01')
        self.format = '%Y%m%d-%H%M_%{{{}}}' # 60 second precision
        self.prefix = 'prefix_'
        self.suffix = '_suffix'


    def mkdte(self, timestamp, length):
        fmt = Format(self.format.format(length))
        return DateTimeEncoding(timestamp, fmt, self.encoder, self.prefix, self.suffix)


    def test_initialization(self):
        length = 1 # not enough for 60 second precision
        expected = 7 # 2**6 = 64 seconds +1 for counter

        for length in (length, expected -1, expected, expected +1, expected +10):
            dte = self.mkdte(self.timestamp, length)
            prefix, date, timecount, suffix = str(dte).split('_')

            self.assertEqual(prefix +'_', self.prefix)
            self.assertEqual(date, self.timestamp.strftime(self.format.split('_')[0]))
            self.assertEqual(len(timecount), expected if length < expected else length)
            self.assertEqual('_'+ suffix, self.suffix)


    def test_renew(self):
        length = 7
        dte = self.mkdte(self.timestamp, length)
        prefix, date, timecount, suffix = str(dte).split('_')
        tc1 = self.encoder.decode(timecount)

        for i in range(1, 128):
            dte.renew()
            prefix, date, timecount, suffix = str(dte).split('_')
            tc2 = self.encoder.decode(timecount)
            bits = floor(log2(i))

            self.assertEqual(len(timecount), length + bits)
            self.assertEqual(tc2, (tc1 << bits) + i)
            self.assertEqual((tc2 - i) >> bits, tc1)

