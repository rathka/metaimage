#!/usr/bin/env python3
from setuptools import setup

verfile = 'src/metaimage/__init__.py'

for line in open(verfile, 'r'):
    if '__version__ = ' in line:
        VERSION = line.split("'")[1]
        break

setup(
    name = "metaimage",
    version = VERSION,
    description = "Batch-processing image files",
    long_description = "Batch-processing tool for image metadata",
    author = "Rasmus Thystrup Karstensen",
    author_email = 'rathka@gmail.com',
    url = "https://gitlab.com/rathka/metaimage",
    download_url = "https://gitlab.com/rathka/metaimage/repository/archive.tar.bz2?ref=master",
    platforms = ['any'],
    license = "GPLv3+",
    packages = ['metaimage'],
    package_dir = {'metaimage': 'src/metaimage'},
    zip_safe = True,
    entry_points = {
        'console_scripts': [
            'metaimage = metaimage.__main__:main',
        ],
    },
    data_files = [
        ('share/doc/metaimage', ['README']),
    ],
)
